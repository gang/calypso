"""Define methods to construct configured SCT Digitization tools and algorithms

Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
"""
from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory
PileUpXingFolder=CompFactory.PileUpXingFolder

from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
from FaserGeoModel.SCTGMConfig import SctGeometryCfg

from FaserSiLorentzAngleTool.FaserSCT_LorentzAngleConfig import FaserSCT_LorentzAngleCfg


def FaserSCT_ClusterizationCommonCfg(flags, name="FaserSCT_ClusterizationToolCommon", **kwargs):
    """Return ComponentAccumulator with common FaserSCT Clusterization tool config"""
    acc = SctGeometryCfg(flags)

    kwargs.setdefault("InputObjectName", "SCT_RDOs")
    Tracker__TrackerClusterMakerTool,Tracker__FaserSCT_ClusteringTool=CompFactory.getComps("Tracker::TrackerClusterMakerTool", "Tracker::FaserSCT_ClusteringTool")
    trackerClusterMakerTool = Tracker__TrackerClusterMakerTool(name = "TrackerClusterMakerTool")
    faserSCT_LorentzAngleTool=acc.popToolsAndMerge(FaserSCT_LorentzAngleCfg(flags))
    clusteringTool = Tracker__FaserSCT_ClusteringTool(name, globalPosAlg = trackerClusterMakerTool, FaserSiLorentzAngleTool=faserSCT_LorentzAngleTool)
    clusteringTool.timeBins = "01X" 
    # attach ToolHandles
    acc.setPrivateTools(clusteringTool)
    return acc 


def FaserSCT_ClusterizationToolCfg(flags, name="FaserSCT_ClusterizationTool", **kwargs):
    """Return ComponentAccumulator with configured FaserSCT Clusterization tool"""
    #kwargs.setdefault("HardScatterSplittingMode", 0)
    return FaserSCT_ClusterizationCommonCfg(flags, name, **kwargs)


def FaserSCT_ClusterizationBasicCfg(flags, **kwargs):
    """Return ComponentAccumulator for FaserSCT Clusterization"""
    acc = ComponentAccumulator()
    clusterTool = acc.popToolsAndMerge(FaserSCT_ClusterizationToolCfg(flags))
    kwargs.setdefault("SCT_ClusteringTool", clusterTool)
    kwargs.setdefault("DataObjectName", "SCT_RDOs")
    kwargs.setdefault("ClustersName", "SCT_ClusterContainer")
    kwargs.setdefault("SCT_FlaggedCondData", "SCT_Flags")
    Tracker__FaserSCT_Clusterization=CompFactory.Tracker.FaserSCT_Clusterization
    acc.addEventAlgo(Tracker__FaserSCT_Clusterization(**kwargs))
    return acc

def FaserSCT_OutputCfg(flags):                                                                                                    
    """Return ComponentAccumulator with Output for SCT. Not standalone."""
    acc = ComponentAccumulator()
    ItemList = ["Tracker::FaserSCT_ClusterContainer#*"]
    acc.merge(OutputStreamCfg(flags, "ESD", ItemList))
    ostream = acc.getEventAlgo("OutputStreamESD")
    ostream.TakeItemsFromInput = True
    return acc


# with output defaults
def FaserSCT_ClusterizationCfg(flags, **kwargs):
    """Return ComponentAccumulator for SCT Clusterization and Output"""
    acc = FaserSCT_ClusterizationBasicCfg(flags, **kwargs)
    acc.merge(FaserSCT_OutputCfg(flags))
    return acc
