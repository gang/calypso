# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

## @file: AtlasGeoModel/python/GeoModelInit.py
## @brief: Encapsulate GeoModel configuration
 
def _setupGeoModel():
    from AthenaCommon.JobProperties import jobproperties
    from AthenaCommon.AppMgr import theApp
    from AthenaCommon.AppMgr import ServiceMgr as svcMgr
 
    import DetDescrCnvSvc.DetStoreConfig
    svcMgr.DetDescrCnvSvc.IdDictFromRDB = True

    # Conditions DB setup and TagInfo
    from IOVDbSvc.CondDB import conddb
    import EventInfoMgt.EventInfoMgtInit
 
    from GeoModelSvc.GeoModelSvcConf import GeoModelSvc
    geoModelSvc = GeoModelSvc()
    svcMgr += geoModelSvc
    theApp.CreateSvc += [ "GeoModelSvc"]

    # Set up detector tools here

    if not hasattr(svcMgr,'ScintGeometryDBSvc'):
        from GeometryDBSvc.GeometryDBSvcConf import GeometryDBSvc
        svcMgr+=GeometryDBSvc("ScintGeometryDBSvc")

    if not hasattr(svcMgr, 'TrackerGeometryDBSvc'):
        from GeometryDBSvc.GeometryDBSvcConf import GeometryDBSvc
        svcMgr+=GeometryDBSvc("TrackerGeometryDBSvc")

    # from AthenaCommon import CfgGetter
    from VetoGeoModel.VetoGeoModelConf import VetoDetectorTool
    vetoDetectorTool = VetoDetectorTool( DetectorName = "Veto",
                                         Alignable = True,
                                         RDBAccessSvc = "RDBAccessSvc",
                                         GeometryDBSvc = "ScintGeometryDBSvc",
                                         GeoDbTagSvc = "GeoDbTagSvc")

    geoModelSvc.DetectorTools += [ vetoDetectorTool ]

    from TriggerGeoModel.TriggerGeoModelConf import TriggerDetectorTool
    triggerDetectorTool = TriggerDetectorTool( DetectorName = "Trigger",
                                               Alignable = True,
                                               RDBAccessSvc = "RDBAccessSvc",
                                               GeometryDBSvc = "ScintGeometryDBSvc",
                                               GeoDbTagSvc = "GeoDbTagSvc")

    geoModelSvc.DetectorTools += [ triggerDetectorTool ]

    from PreshowerGeoModel.PreshowerGeoModelConf import PreshowerDetectorTool
    preshowerDetectorTool = PreshowerDetectorTool( DetectorName = "Preshower",
                                                   Alignable = True,
                                                   RDBAccessSvc = "RDBAccessSvc",
                                                   GeometryDBSvc = "ScintGeometryDBSvc",
                                                   GeoDbTagSvc = "GeoDbTagSvc")

    geoModelSvc.DetectorTools += [ preshowerDetectorTool ]

    from FaserSCT_GeoModel.FaserSCT_GeoModelConf import FaserSCT_DetectorTool
    faserSCTDetectorTool = FaserSCT_DetectorTool( DetectorName = "SCT",
                                                  Alignable = True,
                                                  RDBAccessSvc = "RDBAccessSvc",
                                                  GeometryDBSvc = "TrackerGeometryDBSvc",
                                                  GeoDbTagSvc = "GeoDbTagSvc")

    geoModelSvc.DetectorTools += [ faserSCTDetectorTool ]

    from DipoleGeoModel.DipoleGeoModelConf import DipoleTool
    dipoleTool = DipoleTool( RDBAccessSvc = "RDBAccessSvc",
                             GeometryDBSvc = "TrackerGeometryDBSvc",
                             GeoDbTagSvc = "GeoDbTagSvc")
    geoModelSvc.DetectorTools += [ dipoleTool ]

    pass

## setup GeoModel at module import
_setupGeoModel()

## clean-up: avoid running multiple times this method
del _setupGeoModel
