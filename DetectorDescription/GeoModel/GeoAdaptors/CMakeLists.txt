################################################################################
# Package: GeoAdaptors
################################################################################

# Declare the package name:
atlas_subdir( GeoAdaptors )

# External dependencies:
find_package( CLHEP )

# Component(s) in the package:
atlas_add_library( GeoAdaptors
                   src/*.cxx
                   PUBLIC_HEADERS GeoAdaptors
                   INCLUDE_DIRS ${CLHEP_INCLUDE_DIRS}
                   DEFINITIONS ${CLHEP_DEFINITIONS}
                   LINK_LIBRARIES ${CLHEP_LIBRARIES} Identifier ScintIdentifier ScintReadoutGeometry ScintSimEvent TrackerIdentifier TrackerReadoutGeometry TrackerSimEvent StoreGateLib SGtests )

