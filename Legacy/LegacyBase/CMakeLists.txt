################################################################################
# Package: LegacyBase
# Contains objects written to TDR ntuple
################################################################################


# Declare the package name:
atlas_subdir( LegacyBase )

# External dependencies:
find_package( CLHEP )
find_package( ROOT COMPONENTS Core Tree RIO pthread Physics )
find_package( Geant4 )

atlas_add_library( LegacyBase
		   LegacyBase/*.h src/*.cxx
		   PUBLIC_HEADERS LegacyBase
		   #INCLUDE_DIRS ${GEANT4_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
		   #PRIVATE_INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
		   #LINK_LIBRARIES ${GEANT4_LIBRARIES} ${CLHEP_LIBRARIES}
		   #PRIVATE_LINK_LIBRARIES ${ROOT_LIBRARIES}
		   INCLUDE_DIRS ${GEANT4_INCLUDE_DIRS} ${ROOT_INCLUDE_DIRS} ${CLHEP_INCLUDE_DIRS}
		   LINK_LIBRARIES ${GEANT4_LIBRARIES} ${ROOT_LIBRARIES} ${CLHEP_LIBRARIES}
)

atlas_add_dictionary( LegacyBaseDict
		      LegacyBase/LegacyBaseDict.h
		      LegacyBase/selection.xml
		      INCLUDE_DIRS ${ROOT_INCLUDE_DIRS}
		      LINK_LIBRARIES LegacyBase ${ROOT_LIBRARIES}
#		      DATA_LINKS LegacyBase
 		      )

#atlas_generate_cliddb( LegacyBase )

# Install files from the package:
atlas_install_headers( LegacyBase )
