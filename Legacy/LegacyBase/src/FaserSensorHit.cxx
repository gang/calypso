#include "LegacyBase/FaserSensorHit.h"
#include "G4UnitsTable.hh"
#include "G4VVisManager.hh"
#include "G4Circle.hh"
#include "G4Colour.hh"
#include "G4VisAttributes.hh"
#include "G4RunManager.hh"
#include "G4MTRunManager.hh"

#include <iomanip>

G4ThreadLocal G4Allocator<FaserSensorHit>* FaserSensorHitAllocator = 0;

FaserSensorHit::FaserSensorHit()
  : G4VHit(),
    fPlaneID(-1),
    fModuleID(-1),
    fSensorID(-1),
    fRowID(-1),
    fStripID(-1),
    fEdep(0.0),
    fGlobalPos(G4ThreeVector()),
    fLocalPos(G4ThreeVector()),
    fTransform(G4AffineTransform()),
    fTrackID(-1),
    fEnergy(0),
    fOriginTrackID(-1)
{}

FaserSensorHit::~FaserSensorHit() 
{ }

FaserSensorHit::FaserSensorHit(const FaserSensorHit& right)
  : G4VHit()
{
  fPlaneID = right.fPlaneID;
  fModuleID = right.fModuleID;
  fSensorID = right.fSensorID;
  fRowID = right.fRowID;
  fStripID = right.fStripID;
  fEdep = right.fEdep;
  fGlobalPos = right.fGlobalPos;
  fLocalPos = right.fLocalPos;
  fTransform = right.fTransform;
  
  fTrackID = right.fTrackID;
  fEnergy = right.fEnergy;
  fOriginTrackID = right.fOriginTrackID;
}

const FaserSensorHit& FaserSensorHit::operator=(const FaserSensorHit& right)
{
  fPlaneID = right.fPlaneID;
  fModuleID = right.fModuleID;
  fSensorID = right.fSensorID;
  fRowID = right.fRowID;
  fStripID = right.fStripID;
  fEdep =right.fEdep;
  fGlobalPos = right.fGlobalPos;
  fLocalPos = right.fLocalPos;
  fTransform = right.fTransform;

  fTrackID = right.fTrackID;
  fEnergy = right.fEnergy;
  fOriginTrackID = right.fOriginTrackID;

  return *this;
}

G4int FaserSensorHit::operator==(const FaserSensorHit& right)
{
  return ( this == &right) ? 1 : 0;
}

void FaserSensorHit::Draw()
{ }

void FaserSensorHit::Print()
{
  G4cout 
    << "  trackID: " << fTrackID << " sensorID: " 
    << (10000000*fPlaneID + 1000000*fModuleID + 100000*fSensorID + 10000*fRowID + fStripID) 
    << " Edep: " 
    << std::setw(7) << G4BestUnit(fEdep, "Energy")
    << " Global: " 
    << std::setw(7) << G4BestUnit(fGlobalPos, "Length")
    << " Local: " 
    << std::setw(7) << G4BestUnit(fLocalPos, "Length")
    << G4endl;
}
