To run on Calypso MC data (from an installation (run) directory):

% source ./setup.sh

% export VP1PLUGINPATH=./lib

% vti12 -mc -noautoconf -nosortdbreplicas &lt;input HITS or EVNT file&gt;

Note that VP1PLUGINPATH can be ninja-changed by asetup, and if it does not include the Calypso installation library folder, nothing will work

To run on cosmic ray data (RDO output from the TrackerDataAccess example programs), with the correct geometry, do the following commands instead:

% source ./setup.sh

% export VP1PLUGINPATH=./lib

% vti12 -noautoconf -nosortdbreplicas -detdescr='FASER-CR' &lt;input RDO file&gt;