// Misc includes
#include <bitset>
#include <vector>

// EDM include(s):
#include "xAODCore/AuxStoreAccessorMacros.h"

// Local include(s):
#include "xAODFaserTracking/Track.h"
#include "xAODFaserTracking/FaserTrackSummaryAccessors.h"

namespace xAOD {

  Track::Track()
  : IParticle() {
      
  }
  
  Track::Track(const Track& tp ) 
  : IParticle( tp ) {
    makePrivateStore( tp );
  }
  
  Track& Track::operator=(const Track& tp ){
    if(this == &tp) return *this;
    
    if( ( ! hasStore() ) && ( ! container() ) ) {
       makePrivateStore();
    }
    this->IParticle::operator=( tp );
    return *this;
  }
  
  Track::~Track(){
  }
  
  double Track::pt() const {
  return p4().Pt();
  }
  
  double Track::eta() const {
  return p4().Eta(); 
  }
  
  double Track::phi() const {
  return p4().Phi(); 
  }
  
  double Track::m() const {
  return p4().M();
  }
  
  double Track::e() const {
  return p4().E(); 
  }
  double Track::rapidity() const {
  return p4().Rapidity();
  }

  Track::FourMom_t Track::p4() const {
    Track::FourMom_t p4;
    using namespace std;
    float p = 10.e6; // 10 TeV (default value for very high pt muons, with qOverP==0)
    if (fabs(qOverP())>0.) p = 1/fabs(qOverP());
    float thetaT = theta();
    float phiT = phi();
    float sinTheta= sin(thetaT);
    float px = p*sinTheta*cos(phiT);
    float py = p*sinTheta*sin(phiT);
    float pz = p*cos(thetaT);
    float e  =  pow (139.570,2) + pow( px,2) + pow( py,2) + pow( pz,2);
    p4.SetPxPyPzE( px, py, pz, sqrt(e) ); 
    return p4;
  }

  Type::ObjectType Track::type() const {
     return Type::Other;
  }
  
  FaserType::ObjectType Track::faserType() const {
     return FaserType::Track;
  }

  float Track::charge() const {
    return (qOverP() > 0) ? 1 : ((qOverP() < 0) ? -1 : 0);
  }

  AUXSTORE_PRIMITIVE_GETTER(Track, float, x0)
  AUXSTORE_PRIMITIVE_GETTER(Track, float, y0)
  AUXSTORE_PRIMITIVE_GETTER(Track, float, phi0)
  AUXSTORE_PRIMITIVE_GETTER(Track, float, theta)
  AUXSTORE_PRIMITIVE_GETTER(Track, float, qOverP)

  const DefiningParameters_t Track::definingParameters() const{
    DefiningParameters_t tmp;
    tmp << x0() , y0() , phi0() , theta() , qOverP();
    return tmp;
  }

  void Track::setDefiningParameters(float x0, float y0, float phi0, float theta, float qOverP) {
    static Accessor< float > acc1( "x0" );
    acc1( *this ) = x0;

    static Accessor< float > acc2( "y0" );
    acc2( *this ) = y0;

    static Accessor< float > acc3( "phi0" );
    acc3( *this ) = phi0;

    static Accessor< float > acc4( "theta" );
    acc4( *this ) = theta;

    static Accessor< float > acc5( "qOverP" );
    acc5( *this ) = qOverP;

    return;
  }

  void Track::setDefiningParametersCovMatrix(const xAOD::ParametersCovMatrix_t& cov){

    static Accessor< std::vector<float> > acc( "definingParametersCovMatrix" );
    FMath::compress(cov,acc(*this));
  }

  const xAOD::ParametersCovMatrix_t Track::definingParametersCovMatrix() const {
    xAOD::ParametersCovMatrix_t cov; 
    const std::vector<float>& covVec = definingParametersCovMatrixVec();
    if( !covVec.empty() ) FMath::expand( covVec.begin(), covVec.end(),cov );
    else cov.setIdentity();
    return cov;
  }

  const std::vector<float>& Track::definingParametersCovMatrixVec() const {
  // Can't use AUXSTORE_PRIMITIVE_SETTER_AND_GETTER since I have to add Vec to the end of setDefiningParametersCovMatrix to avoid clash.
    static Accessor< std::vector<float> > acc( "definingParametersCovMatrix" );
    return acc(*this);
  }

  void Track::setDefiningParametersCovMatrixVec(const std::vector<float>& cov){
  // Can't use AUXSTORE_PRIMITIVE_SETTER_AND_GETTER since I have to add Vec to the end of setDefiningParametersCovMatrix to avoid clash.
    static Accessor< std::vector<float> > acc( "definingParametersCovMatrix" );
    acc(*this)=cov;
  }

  AUXSTORE_PRIMITIVE_GETTER(Track, float, vx)
  AUXSTORE_PRIMITIVE_GETTER(Track, float, vy)
  AUXSTORE_PRIMITIVE_GETTER(Track, float, vz)

  void Track::setParametersOrigin(float x, float y, float z){
    static Accessor< float > acc1( "vx" );
    acc1( *this ) = x;

    static Accessor< float > acc2( "vy" );
    acc2( *this ) = y;

    static Accessor< float > acc3( "vz" );
    acc3( *this ) = z;
  }

  AUXSTORE_PRIMITIVE_GETTER(Track, float, chiSquared)
  AUXSTORE_PRIMITIVE_GETTER(Track, float, numberDoF)

  void Track::setFitQuality(float chiSquared, float numberDoF){
    static Accessor< float > acc1( "chiSquared" );
    acc1( *this ) = chiSquared;  
    static Accessor< float > acc2( "numberDoF" );
    acc2( *this ) = numberDoF;   
  }

  static SG::AuxElement::Accessor< Track::StripClusterLinks_t > clusterAcc( "clusterLinks" );
  AUXSTORE_OBJECT_SETTER_AND_GETTER( Track, Track::StripClusterLinks_t, clusterLinks, setClusterLinks )
  AUXSTORE_PRIMITIVE_SETTER_AND_GETTER(Track, uint32_t, hitPattern, setHitPattern)

  size_t Track::numberOfParameters() const{
    ///@todo - Can we do this in a better way? Not great to force retrieval of one specific parameter - any would do.
    static Accessor< std::vector<float>  > acc( "parameterX" );
    if(! acc.isAvailable( *this )) return 0;
    return acc(*this).size();
  }

  const CurvilinearParameters_t Track::trackParameters(unsigned int index) const{
    CurvilinearParameters_t tmp;
    tmp << parameterX(index),parameterY(index),parameterZ(index),
      parameterPX(index),parameterPY(index),parameterPZ(index);
    return tmp;
  }

  void Track::setTrackParameters(std::vector<std::vector<float> >& parameters){
    static Accessor< std::vector < float > > acc1( "parameterX" );
    static Accessor< std::vector < float > > acc2( "parameterY" );
    static Accessor< std::vector < float > > acc3( "parameterZ" );
    static Accessor< std::vector < float > > acc4( "parameterPX" );
    static Accessor< std::vector < float > > acc5( "parameterPY" );
    static Accessor< std::vector < float > > acc6( "parameterPZ" );

    static Accessor< std::vector<uint8_t>  > acc7( "parameterPosition" );

    acc1(*this).resize(parameters.size());
    acc2(*this).resize(parameters.size());
    acc3(*this).resize(parameters.size());
    acc4(*this).resize(parameters.size());
    acc5(*this).resize(parameters.size());
    acc6(*this).resize(parameters.size());

    acc7(*this).resize(parameters.size());

    unsigned int index=0;
    // std::cout<<"Adding this many parameters: "<<parameters.size()<<std::endl;
    std::vector<std::vector<float> >::const_iterator it=parameters.begin(), itEnd=parameters.end();
    for (;it!=itEnd;++it,++index){
      assert((*it).size()==6);
      acc1(*this).at(index)=(*it).at(0);
      acc2(*this).at(index)=(*it).at(1);
      acc3(*this).at(index)=(*it).at(2);
      acc4(*this).at(index)=(*it).at(3);
      acc5(*this).at(index)=(*it).at(4);
      acc6(*this).at(index)=(*it).at(5);
      // std::cout<<"param=("<<(*it).at(0)<<", "<<(*it).at(0)<<", "<<(*it).at(1)<<", "<<(*it).at(2)<<", "<<(*it).at(3)<<", "<<(*it).at(4)<<", "<<(*it).at(5)<<")"<<std::endl;
    }
  }

  float Track::parameterX(unsigned int index) const  {
    static Accessor< std::vector<float>  > acc( "parameterX" );
    return acc(*this).at(index);
  }

  float Track::parameterY(unsigned int index) const  {
    static Accessor< std::vector<float>  > acc( "parameterY" );
    return acc(*this).at(index);
  }

  float Track::parameterZ(unsigned int index) const  {
    static Accessor< std::vector<float>  > acc( "parameterZ" );
    return acc(*this).at(index);
  }

  float Track::parameterPX(unsigned int index) const {
    static Accessor< std::vector<float>  > acc( "parameterPX" );
    return acc(*this).at(index);
  }

  float Track::parameterPY(unsigned int index) const {
    static Accessor< std::vector<float>  > acc( "parameterPY" );
    return acc(*this).at(index);
  }

  float Track::parameterPZ(unsigned int index) const {
    static Accessor< std::vector<float>  > acc( "parameterPZ" );    
    return acc(*this).at(index);
  }

  xAOD::ParametersCovMatrix_t Track::trackParameterCovarianceMatrix(unsigned int index) const
  {
    static Accessor< std::vector<float>  > acc( "trackParameterCovarianceMatrices" );
    unsigned int offset = index*15;
    // copy the correct values into the temp matrix
    xAOD::ParametersCovMatrix_t tmp;
    std::vector<float>::const_iterator it = acc(*this).begin()+offset;
    FMath::expand(it,it+15,tmp);
    return tmp;
  }

  void Track::setTrackParameterCovarianceMatrix(unsigned int index, std::vector<float>& cov){
    assert(cov.size()==15);
    unsigned int offset = index*15;
    static Accessor< std::vector < float > > acc( "trackParameterCovarianceMatrices" );
    std::vector<float>& v = acc(*this);
    v.resize(offset+15);
    std::copy(cov.begin(),cov.end(),v.begin()+offset );
  }

  xAOD::FaserParameterPosition Track::parameterPosition(unsigned int index) const
  {
    static Accessor< std::vector<uint8_t>  > acc( "parameterPosition" );
    return static_cast<xAOD::FaserParameterPosition>(acc(*this).at(index));
  }

  bool Track::indexOfParameterAtPosition(unsigned int& index, FaserParameterPosition position) const 
  {
    size_t maxParameters = numberOfParameters();
    bool foundParameters=false;
    for (size_t i=0; i<maxParameters; ++i){
      if (parameterPosition(i)==position){
        foundParameters=true;
        index=i;
        break;
      }
    }
    return foundParameters;
  }

  void  Track::setParameterPosition(unsigned int index, xAOD::FaserParameterPosition pos){
    static Accessor< std::vector<uint8_t>  > acc( "parameterPosition" );
    acc( *this ).at(index) = static_cast<uint8_t>(pos);
  }

  AUXSTORE_PRIMITIVE_GETTER_WITH_CAST(Track, uint8_t, xAOD::FaserTrackFitter,trackFitter)
  AUXSTORE_PRIMITIVE_SETTER_WITH_CAST(Track, uint8_t, xAOD::FaserTrackFitter,trackFitter, setTrackFitter)

  AUXSTORE_PRIMITIVE_SETTER_WITH_CAST(Track, uint8_t, xAOD::FaserParticleHypothesis, particleHypothesis, setParticleHypothesis)
  AUXSTORE_PRIMITIVE_GETTER_WITH_CAST(Track, uint8_t, xAOD::FaserParticleHypothesis, particleHypothesis)

  bool Track::summaryValue(uint8_t& value, const FaserSummaryType &information)  const {
    xAOD::Track::Accessor< uint8_t >* acc = faserTrackSummaryAccessor<uint8_t>( information );
    if( ( ! acc ) || ( ! acc->isAvailable( *this ) ) ) return false;
  // Retrieve the value:
    value = ( *acc )( *this );
    return true;
  }
  
  void Track::setSummaryValue(uint8_t& value, const FaserSummaryType &information){
    xAOD::Track::Accessor< uint8_t >* acc = faserTrackSummaryAccessor<uint8_t>( information );
  // Set the value:
    ( *acc )( *this ) = value;
  }

} // namespace xAOD