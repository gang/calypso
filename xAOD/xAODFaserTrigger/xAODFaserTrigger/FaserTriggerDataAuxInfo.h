// Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2020 CERN for the benefit of the FASER collaboration
*/

#ifndef XAODFASERTRIGGER_FASERTRIGGERDATAAUXINFO_H
#define XAODFASERTRIGGER_FASERTRIGGERDATAAUXINFO_H

// Local include(s):
#include "xAODFaserTrigger/versions/FaserTriggerDataAuxInfo_v1.h"

namespace xAOD {
  /// Declare the latest version of the class
  typedef FaserTriggerDataAuxInfo_v1 FaserTriggerDataAuxInfo;
}

// Set up a CLID for the container:
#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::FaserTriggerDataAuxInfo, 82583928, 1 )

#endif // XAODFASERTRIGGER_FASERTRIGGERDATAAUXINFO_H
