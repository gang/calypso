# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

#!/usr/bin/env python
import sys
from AthenaConfiguration.ComponentFactory import CompFactory
from AthenaCommon.Constants import VERBOSE, INFO

def TriggerDataAccessExampleCfg(flags, name="TriggerDataAccessExampleAlg", **kwargs):

    from FaserGeoModel.FaserGeoModelConfig import FaserGeometryCfg
    a = FaserGeometryCfg(flags)

    TriggerDataAccessExampleAlg = CompFactory.FaserTriggerDataAccess
    a.addEventAlgo(TriggerDataAccessExampleAlg(name, **kwargs))
    
    from FaserByteStreamCnvSvc.FaserByteStreamCnvSvcConfig import FaserByteStreamCnvSvcCfg
    a.merge(FaserByteStreamCnvSvcCfg(flags))

    # thistSvc = CompFactory.THistSvc()
    # thistSvc.Output += ["HIST DATAFILE='rdoReadHist.root' OPT='RECREATE'"]
    # a.addService(thistSvc)
    
    return a


if __name__ == "__main__":
    from AthenaCommon.Configurable import Configurable
    from CalypsoConfiguration.AllConfigFlags import ConfigFlags

    Configurable.configurableRun3Behavior = True
    
# Flags for this job
    ConfigFlags.Input.isMC = False                               # Needed to bypass autoconfig
    ConfigFlags.IOVDb.GlobalTag = "OFLCOND-XXXX-XXX-XX"          # Needed to bypass autoconfig, only the "OFLCOND" matters at the moment
    ConfigFlags.IOVDb.DatabaseInstance = "OFLP200"               # Use MC conditions for now
    ConfigFlags.GeoModel.FaserVersion     = "FASER-01"           # Default FASER geometry
    ConfigFlags.Input.ProjectName = "data20"
    ConfigFlags.GeoModel.Align.Dynamic    = False
    ConfigFlags.Input.Files = ["cosmics.raw"]
    ConfigFlags.Output.RDOFileName = "trigger.RDO.pool.root"
    ConfigFlags.lock()

# Configure components
    from AthenaConfiguration.MainServicesConfig import MainServicesCfg
    from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
    
    acc = MainServicesCfg(ConfigFlags)
    acc.merge(PoolWriteCfg(ConfigFlags))

# Configure output

    from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
    itemList = [ "xAOD::EventInfo#*",
                 "xAOD::EventAuxInfo#*",
                 "xAOD::FaserTriggerData#*",
                 "xAOD::FaserTriggerDataAuxInfo#*" ]
    acc.merge(OutputStreamCfg(ConfigFlags, "RDO", itemList))
    ostream = acc.getEventAlgo("OutputStreamRDO")

# Set up algorithm
    acc.merge(TriggerDataAccessExampleCfg(ConfigFlags))

# Configure verbosity    
    # ConfigFlags.dump()
    # logging.getLogger('forcomps').setLevel(VERBOSE)
    acc.foreach_component("*").OutputLevel = VERBOSE
    acc.foreach_component("*ClassID*").OutputLevel = INFO
    # log.setLevel(VERBOSE)

    # acc.getService("FaserByteStreamInputSvc").DumpFlag = True
    acc.getService("FaserEventSelector").OutputLevel = VERBOSE
    acc.getService("FaserByteStreamInputSvc").OutputLevel = VERBOSE
    acc.getService("FaserByteStreamCnvSvc").OutputLevel = VERBOSE
    acc.getService("FaserByteStreamAddressProviderSvc").OutputLevel = VERBOSE
    acc.getService("MessageSvc").Format = "% F%40W%S%7W%R%T %0W%M"
# Execute and finish
    sys.exit(int(acc.run(maxEvents=-1).isFailure()))
