/*
  Copyright (C) 2020 CERN for the benefit of the FASER collaboration
*/

/*
 * ScintWaveformAccess.h
 * 
 * Simple algorithm to access digitizer data from storegate.
 * Try to write a proper thread-safe algorithm.
 *
 */

#ifndef WAVEFORMDATAACCESSEXAMPLE_SCINTWAVEFORMACCESS_H
#define WAVEFORMDATAACCESSEXAMPLE_SCINTWAVEFORMACCESS_H 

#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "StoreGate/ReadHandleKey.h"

#include "ScintRawEvent/ScintWaveformContainer.h"

class ScintWaveformAccess: public AthReentrantAlgorithm
{
 public:
  ScintWaveformAccess(const std::string& name, ISvcLocator* pSvcLocator);
  virtual ~ScintWaveformAccess();

  virtual StatusCode initialize() override;
  virtual StatusCode execute(const EventContext& ctx) const override;
  virtual StatusCode finalize() override;

 private:
  /// StoreGate key
  SG::ReadHandleKey<ScintWaveformContainer> m_CaloWaveformContainer
    { this, "CaloWaveformContainerKey", "CaloWaveforms", "ReadHandleKey for CaloWaveforms Container"};
  SG::ReadHandleKey<ScintWaveformContainer> m_VetoWaveformContainer
    { this, "VetoWaveformContainerKey", "VetoWaveforms", "ReadHandleKey for CaloWaveforms Container"};
  SG::ReadHandleKey<ScintWaveformContainer> m_TriggerWaveformContainer
    { this, "TriggerWaveformContainerKey", "TriggerWaveforms", "ReadHandleKey for TriggerWaveforms Container"};
  SG::ReadHandleKey<ScintWaveformContainer> m_PreshowerWaveformContainer
    { this, "PreshowerWaveformContainerKey", "PreshowerWaveforms", "ReadHandleKey for PreshowerWaveforms Container"};
  SG::ReadHandleKey<ScintWaveformContainer> m_TestWaveformContainer
    { this, "TestWaveformContainerKey", "TestWaveforms", "ReadHandleKey for TestWaveforms Container"};
  SG::ReadHandleKey<ScintWaveformContainer> m_ClockWaveformContainer
    { this, "ClockWaveformContainerKey", "ClockWaveforms", "ReadHandleKey for ClockWaveforms Container"};
};

#endif /* WAVEFORMDATAACCESSEXAMPLE_SCINTWAVEFORMACCESS_H */
