/*
  Copyright (C) 2020 CERN for the benefit of the FASER collaboration
*/

#include "ScintWaveformCnv_p0.h"

void
ScintWaveformCnv_p0::persToTrans(const ScintWaveform_p0* persObj, ScintWaveform* transObj, MsgStream& log) {

  // Just fill available data here
  // Rest of it patched up in ScintWaveformContainerCnv_p0
  transObj->setIdentifier(persObj->m_ID);
  transObj->setChannel(persObj->m_channel);
  transObj->setCounts(persObj->m_adc_counts);

}

void
ScintWaveformCnv_p0::transToPers(const ScintWaveform* transObj, ScintWaveform_p0* persObj, MsgStream& log) {

  // log << MSG::DEBUG << "ScintWaveformCnv_p0::transToPers called" << endmsg;
  // log << MSG::DEBUG << "Transient waveform:" << endmsg;
  // log << MSG::DEBUG << (*transObj) << endmsg;
  // log << MSG::DEBUG << "Persistent waveform (before):" << endmsg;
  // persObj->print();
  persObj->m_ID = transObj->identify();
  persObj->m_channel = transObj->channel();

  // Use copy here
  persObj->m_adc_counts = transObj->adc_counts();

  // log << MSG::DEBUG << "Persistent waveform (after):" << endmsg;
  // persObj->print();
  // log << MSG::DEBUG << "ScintWaveformCnv_p0::transToPers done" << endmsg;

}
