/*
 Copyright 2020 CERN for the benefit of the FASER collaboration
*/

#ifndef SCINTWAVEFORMCONTAINERCNV_P0_H
#define SCINTWAVEFORMCONTAINERCNV_P0_H

#include "AthenaPoolCnvSvc/T_AthenaPoolTPConverter.h"

#include "ScintRawEvent/ScintWaveformContainer.h"
#include "ScintEventAthenaPool/ScintWaveformContainer_p0.h"

class ScintWaveformContainerCnv_p0 : public T_AthenaPoolTPCnvBase<ScintWaveformContainer, ScintWaveformContainer_p0> {

 public:
  ScintWaveformContainerCnv_p0() {};

  virtual void persToTrans(const ScintWaveformContainer_p0* persCont,
			   ScintWaveformContainer* transCont, 
			   MsgStream& log);

  virtual void transToPers(const ScintWaveformContainer* transCont, 
			   ScintWaveformContainer_p0* persCont, 
			   MsgStream& log);

};

#endif
