//Dear emacs, this is -*- c++ -*-

/*
  Copyright (C) 2020 CERN for the benefit of the ATLAS collaboration
*/

#ifndef SCINTBYTESTREAM_SCINTBYTESTREAMCNV_H
#define SCINTBYTESTREAM_SCINTBYTESTREAMCNV_H

#include "GaudiKernel/ClassID.h"
#include "GaudiKernel/Converter.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ToolHandle.h"

#include "AthenaBaseComps/AthMessaging.h"
#include "FaserByteStreamCnvSvcBase/FaserByteStreamAddress.h"

class ScintWaveformDecoderTool;
class IFaserROBDataProviderSvc;

// Abstract factory to create the converter
template <class TYPE> class CnvFactory;

class ScintByteStreamCnv: public Converter, public AthMessaging {

public: 
  ScintByteStreamCnv(ISvcLocator* svcloc);
  virtual ~ScintByteStreamCnv();
  
  virtual StatusCode initialize() override;
  virtual StatusCode finalize() override;
  
  virtual StatusCode createObj(IOpaqueAddress* pAddr, DataObject*& pObj) override;  
  /// Storage type and class ID
  virtual long repSvcType() const override { return i_repSvcType(); }
  static long storageType() { return FaserByteStreamAddress::storageType(); }
  static const CLID& classID();
  
private:
  std::string m_name;
  ToolHandle<ScintWaveformDecoderTool> m_tool;
  ServiceHandle<IFaserROBDataProviderSvc> m_rdpSvc;

};

#endif  /* SCINTBYTESTREAM_SCINTBYTESTREAMCNV_H */


