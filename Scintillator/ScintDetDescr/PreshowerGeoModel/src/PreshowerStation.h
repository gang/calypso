/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

#ifndef PRESHOWERGEOMODEL_PRESHOWERSTATION_H
#define PRESHOWERGEOMODEL_PRESHOWERSTATION_H

#include "PreshowerComponentFactory.h"

class GeoVPhysVol;
class GeoFullPhysVol;
class GeoLogVol;
class PreshowerIdentifier;
class PreshowerPlate;

class PreshowerStation : public PreshowerUniqueComponentFactory
{

public:
  PreshowerStation(const std::string & name,
             PreshowerPlate* plate,
             ScintDD::PreshowerDetectorManager* detectorManager,
             const PreshowerGeometryManager* geometryManager,
             PreshowerMaterialManager* materials);
  virtual GeoVPhysVol * build(PreshowerIdentifier id);

public:
  int    numPlates()   const {return m_numPlates;}
  double platePitch()  const {return m_platePitch;}
  double thickness()   const {return m_thickness;}
  double width()       const {return m_width;}
  double length()      const {return m_length;}
 
private:
  void getParameters();
  virtual const GeoLogVol * preBuild();
 
  PreshowerPlate*  m_plate;

  int         m_numPlates;
  double      m_platePitch;

  double      m_thickness;
  double      m_width;
  double      m_length;

  double      m_safety;
};

#endif // PRESHOWERGEOMODEL_PRESHOWERSTATION_H
