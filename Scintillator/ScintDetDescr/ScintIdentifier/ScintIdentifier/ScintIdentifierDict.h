/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

/**
 * @file ScintIdentifierDict.h
 *
 * @brief This file includes the class for dictionary definitions
 *
 * $Id: $
 */
#ifndef SCINTIDENTIFIER_SCINTIDENTIFIERDICT_H
#define SCINTIDENTIFIER_SCINTIDENTIFIERDICT_H 

// #include "ScintIdentifier/PreshowerID.h"
// #include "ScintIdentifier/ScintillatorID.h"
#include "ScintIdentifier/TriggerID.h"
#include "ScintIdentifier/VetoID.h"

#endif // SCINTIDENTIFIER_SCINTIDENTIFIERDICT_H 
