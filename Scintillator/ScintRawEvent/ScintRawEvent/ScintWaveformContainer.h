/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef SCINTRAWEVENT_SCINTWAVEFORMCONTAINER_H
#define SCINTRAWEVENT_SCINTWAVEFORMCONTAINER_H

#include "ScintRawEvent/ScintWaveform.h"
#include "AthContainers/DataVector.h"
#include "AthenaKernel/CLASS_DEF.h"

// Make this a class in case we need to add some functions
class ScintWaveformContainer : public DataVector<ScintWaveform> {
 public:
  void print() const;
};

std::ostream 
&operator<<(std::ostream &out, const ScintWaveformContainer &container);

CLASS_DEF(ScintWaveformContainer, 1215612331, 1 )
SG_BASE(ScintWaveformContainer, DataVector<ScintWaveform>);

#endif // SCINTRAWEVENT_SCINTWAVEFORMCONTAINER_H
