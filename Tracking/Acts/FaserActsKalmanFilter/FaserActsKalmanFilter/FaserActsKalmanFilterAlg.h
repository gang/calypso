/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#ifndef FASERACTSKALMANFILTER_FASERACTSKALMANFILTERALG_H
#define FASERACTSKALMANFILTER_FASERACTSKALMANFILTERALG_H

// ATHENA
#include "AthenaBaseComps/AthAlgorithm.h"
#include "AthenaBaseComps/AthReentrantAlgorithm.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/Property.h"  /*no forward decl: typedef*/
#include "GaudiKernel/ISvcLocator.h"
#include "StoreGate/StoreGateSvc.h"
#include "StoreGate/ReadCondHandleKey.h"
#include "TrackerPrepRawData/FaserSCT_ClusterCollection.h"
#include "TrkSpacePoint/SpacePoint.h"
#include "MagFieldConditions/FaserFieldCacheCondObj.h"
#include "MagFieldElements/FaserFieldCache.h"
#include "TrackerReadoutGeometry/SiDetectorElementCollection.h"
#include "TrackerSpacePoint/SpacePointForSeedCollection.h"
#include "Identifier/Identifier.h"
#include "GeneratorObjects/McEventCollection.h"
#include "TrackerSimData/TrackerSimDataCollection.h"

// ACTS
#include "Acts/MagneticField/ConstantBField.hpp"
#include "Acts/MagneticField/InterpolatedBFieldMap.hpp"
#include "Acts/MagneticField/SharedBField.hpp"
#include "Acts/Propagator/EigenStepper.hpp"
#include "Acts/Propagator/Propagator.hpp"
#include "Acts/Propagator/detail/SteppingLogger.hpp"
#include "Acts/Fitter/KalmanFitter.hpp"
#include "Acts/Geometry/TrackingGeometry.hpp"
#include "Acts/EventData/TrackParameters.hpp"
#include "Acts/Geometry/GeometryID.hpp"
#include "Acts/Utilities/Helpers.hpp"
#include "Acts/Utilities/Definitions.hpp"

// PACKAGE
#include "FaserActsGeometry/FASERMagneticFieldWrapper.h"
#include "FaserActsGeometry/FaserActsTrackingGeometryTool.h"
#include "FaserActsGeometryInterfaces/IFaserActsTrackingGeometryTool.h"
#include "FaserActsGeometryInterfaces/IFaserActsExtrapolationTool.h"
#include "FaserActsKalmanFilter/FaserActsRecSourceLink.h"
#include "FaserActsKalmanFilter/FaserActsRecMultiTrajectory.h"

// STL
#include <memory>
#include <vector>
#include <fstream>
#include <mutex>

class EventContext;
class IAthRNGSvc;
class FaserSCT_ID;
class TTree;

namespace ActsExtrapolationDetail {
  class VariantPropagator;
}

namespace TrackerDD {
  class SCT_DetectorManager;
}

using TrajectoryContainer = std::vector<FaserActsRecMultiTrajectory>;

//class FaserActsKalmanFilterAlg : public AthReentrantAlgorithm {
class FaserActsKalmanFilterAlg : public AthAlgorithm {
public:
  FaserActsKalmanFilterAlg (const std::string& name, ISvcLocator* pSvcLocator);
  StatusCode initialize() override;
  //StatusCode execute(const EventContext& ctx) const override;
  StatusCode execute() override;
  StatusCode finalize() override;
 
  using FitterResult = Acts::Result<Acts::KalmanFitterResult<RecSourceLink>>;
  // Fit function that takes input measurements, initial trackstate and fitter
  // options and returns some fit-specific result.
  using FitterFunction = std::function<FitterResult(
     const std::vector<RecSourceLink>&,
     const Acts::CurvilinearParameters&,
     const Acts::KalmanFitterOptions<Acts::VoidOutlierFinder>&)>;

  // Create the fitter function implementation.
  static FitterFunction
  makeFitterFunction(
     ActsExtrapolationDetail::VariantPropagator* varProp);

  virtual
  Acts::MagneticFieldContext
  //getMagneticFieldContext(const EventContext& ctx) const;
  getMagneticFieldContext() const;

  void initializeTree();

  void fillFitResult(const Acts::GeometryContext& geoctx, const TrajectoryContainer& trajectories, const Acts::BoundParameters& truthParam);

  void clearTrackVariables();

private:
  const FaserSCT_ID* m_idHelper{nullptr};

  // Read handle for conditions object to get the field cache
  SG::ReadCondHandleKey<FaserFieldCacheCondObj> m_fieldCondObjInputKey {this, "FaserFieldCacheCondObj", "fieldCondObj", "Name of the Magnetic Field conditions object key"};

  ToolHandle<IFaserActsExtrapolationTool> m_extrapolationTool{this, "ExtrapolationTool", "FaserActsExtrapolationTool"};

  Gaudi::Property<std::string> m_fieldMode{this, "FieldMode", "FASER"};
  Gaudi::Property<std::vector<double>> m_constantFieldVector{this, "ConstantFieldVector", {0, 0, 0}};

  SG::ReadHandleKey<SpacePointForSeedCollection>  m_seed_spcollectionKey{this, "FaserSpacePointsSeedsName", "SpacePointForSeedCollection", "SpacePointForSeedCollection"};

    SG::ReadHandleKey<McEventCollection> m_mcEventKey       { this, "McEventCollection", "GEN_EVENT" };
    SG::ReadHandleKey<TrackerSimDataCollection> m_sctMap {this, "TrackerSimDataCollection", "SCT_SDO_Map"};
    const TrackerDD::SCT_DetectorManager* m_detManager{nullptr};

  ServiceHandle<ITHistSvc> m_thistSvc;

  TTree *m_trackTree{nullptr};

  /// Acts tree values
  int m_eventNr{0};
  int m_trajNr{0};
  int m_trackNr{0};

  unsigned long m_t_barcode{0};  /// Truth particle barcode
  int m_t_charge{0};             /// Truth particle charge
  float m_t_eT{0};               /// Truth particle time on the first layer
  float m_t_eLOC0{-99.};         /// Truth local x on the first layer
  float m_t_eLOC1{-99.};         /// Truth local y on the first layer
  float m_t_eTHETA{-99.};        /// Truth particle momentum theta on the first layer
  float m_t_ePHI{-99.};          /// Truth particle momentum phi on the first layer
  float m_t_eQOP{-99.};          /// Truth particle momentum qop on the first layer
  float m_t_x{-99.};             /// Truth particle position x on the first layer
  float m_t_y{-99.};             /// Truth particle position y on the first layer
  float m_t_z{-99.};             /// Truth particle position z on the first layer
  float m_t_px{-99.};            /// Truth particle momentum px on the first layer
  float m_t_py{-99.};            /// Truth particle momentum py on the first layer
  float m_t_pz{-99.};            /// Truth particle momentum pz on the first layer

  int m_nHoles{0};                  /// number of holes in the track fit
  int m_nOutliers{0};               /// number of outliers in the track fit
  int m_nStates{0};                 /// number of all states
  int m_nMeasurements{0};           /// number of states with measurements
  std::vector<int> m_volumeID;      /// volume identifier
  std::vector<int> m_layerID;       /// layer identifier
  std::vector<int> m_moduleID;      /// surface identifier
  std::vector<float> m_lx_hit;      /// uncalibrated measurement local x
  std::vector<float> m_ly_hit;      /// uncalibrated measurement local y
  std::vector<float> m_x_hit;       /// uncalibrated measurement global x
  std::vector<float> m_y_hit;       /// uncalibrated measurement global y
  std::vector<float> m_z_hit;       /// uncalibrated measurement global z
  std::vector<float> m_res_x_hit;   /// hit residual x
  std::vector<float> m_res_y_hit;   /// hit residual y
  std::vector<float> m_err_x_hit;   /// hit err x
  std::vector<float> m_err_y_hit;   /// hit err y
  std::vector<float> m_pull_x_hit;  /// hit pull x
  std::vector<float> m_pull_y_hit;  /// hit pull y
  std::vector<int> m_dim_hit;       /// dimension of measurement

  bool m_hasFittedParams{false};  /// if the track has fitted parameter
  float m_eLOC0_fit{-99.};        /// fitted parameter eLOC_0
  float m_eLOC1_fit{-99.};        /// fitted parameter eLOC_1
  float m_ePHI_fit{-99.};         /// fitted parameter ePHI
  float m_eTHETA_fit{-99.};       /// fitted parameter eTHETA
  float m_eQOP_fit{-99.};         /// fitted parameter eQOP
  float m_eT_fit{-99.};           /// fitted parameter eT
  float m_err_eLOC0_fit{-99.};    /// fitted parameter eLOC_-99.err
  float m_err_eLOC1_fit{-99.};    /// fitted parameter eLOC_1 err
  float m_err_ePHI_fit{-99.};     /// fitted parameter ePHI err
  float m_err_eTHETA_fit{-99.};   /// fitted parameter eTHETA err
  float m_err_eQOP_fit{-99.};     /// fitted parameter eQOP err
  float m_err_eT_fit{-99.};       /// fitted parameter eT err
  float m_px_fit{-99.};           /// fitted parameter global px
  float m_py_fit{-99.};           /// fitted parameter global py
  float m_pz_fit{-99.};           /// fitted parameter global pz
  float m_x_fit{-99.};            /// fitted parameter global PCA x
  float m_y_fit{-99.};            /// fitted parameter global PCA y
  float m_z_fit{-99.};            /// fitted parameter global PCA z
  float m_chi2_fit{-99.};         /// fitted parameter chi2
  float m_ndf_fit{-99.};          /// fitted parameter ndf
  int m_charge_fit{-99};          /// fitted parameter charge

  int m_nPredicted{0};                   /// number of states with predicted parameter
  std::vector<bool> m_prt;               /// predicted status
  std::vector<float> m_eLOC0_prt;        /// predicted parameter eLOC0
  std::vector<float> m_eLOC1_prt;        /// predicted parameter eLOC1
  std::vector<float> m_ePHI_prt;         /// predicted parameter ePHI
  std::vector<float> m_eTHETA_prt;       /// predicted parameter eTHETA
  std::vector<float> m_eQOP_prt;         /// predicted parameter eQOP
  std::vector<float> m_eT_prt;           /// predicted parameter eT
  std::vector<float> m_res_eLOC0_prt;    /// predicted parameter eLOC0 residual
  std::vector<float> m_res_eLOC1_prt;    /// predicted parameter eLOC1 residual
  std::vector<float> m_err_eLOC0_prt;    /// predicted parameter eLOC0 error
  std::vector<float> m_err_eLOC1_prt;    /// predicted parameter eLOC1 error
  std::vector<float> m_err_ePHI_prt;     /// predicted parameter ePHI error
  std::vector<float> m_err_eTHETA_prt;   /// predicted parameter eTHETA error
  std::vector<float> m_err_eQOP_prt;     /// predicted parameter eQOP error
  std::vector<float> m_err_eT_prt;       /// predicted parameter eT error
  std::vector<float> m_pull_eLOC0_prt;   /// predicted parameter eLOC0 pull
  std::vector<float> m_pull_eLOC1_prt;   /// predicted parameter eLOC1 pull
  std::vector<float> m_x_prt;            /// predicted global x
  std::vector<float> m_y_prt;            /// predicted global y
  std::vector<float> m_z_prt;            /// predicted global z
  std::vector<float> m_px_prt;           /// predicted momentum px
  std::vector<float> m_py_prt;           /// predicted momentum py
  std::vector<float> m_pz_prt;           /// predicted momentum pz
  std::vector<float> m_eta_prt;          /// predicted momentum eta
  std::vector<float> m_pT_prt;           /// predicted momentum pT

  int m_nFiltered{0};                    /// number of states with filtered parameter
  std::vector<bool> m_flt;               /// filtered status
  std::vector<float> m_eLOC0_flt;        /// filtered parameter eLOC0
  std::vector<float> m_eLOC1_flt;        /// filtered parameter eLOC1
  std::vector<float> m_ePHI_flt;         /// filtered parameter ePHI
  std::vector<float> m_eTHETA_flt;       /// filtered parameter eTHETA
  std::vector<float> m_eQOP_flt;         /// filtered parameter eQOP
  std::vector<float> m_eT_flt;           /// filtered parameter eT
  std::vector<float> m_res_eLOC0_flt;    /// filtered parameter eLOC0 residual
  std::vector<float> m_res_eLOC1_flt;    /// filtered parameter eLOC1 residual
  std::vector<float> m_err_eLOC0_flt;    /// filtered parameter eLOC0 error
  std::vector<float> m_err_eLOC1_flt;    /// filtered parameter eLOC1 error
  std::vector<float> m_err_ePHI_flt;     /// filtered parameter ePHI error
  std::vector<float> m_err_eTHETA_flt;   /// filtered parameter eTHETA error
  std::vector<float> m_err_eQOP_flt;     /// filtered parameter eQOP error
  std::vector<float> m_err_eT_flt;       /// filtered parameter eT error
  std::vector<float> m_pull_eLOC0_flt;   /// filtered parameter eLOC0 pull
  std::vector<float> m_pull_eLOC1_flt;   /// filtered parameter eLOC1 pull
  std::vector<float> m_x_flt;            /// filtered global x
  std::vector<float> m_y_flt;            /// filtered global y
  std::vector<float> m_z_flt;            /// filtered global z
  std::vector<float> m_px_flt;           /// filtered momentum px
  std::vector<float> m_py_flt;           /// filtered momentum py
  std::vector<float> m_pz_flt;           /// filtered momentum pz
  std::vector<float> m_eta_flt;          /// filtered momentum eta
  std::vector<float> m_pT_flt;           /// filtered momentum pT
  std::vector<float> m_chi2;             /// chisq from filtering

  int m_nSmoothed{0};                    /// number of states with smoothed parameter
  std::vector<bool> m_smt;               /// smoothed status
  std::vector<float> m_eLOC0_smt;        /// smoothed parameter eLOC0
  std::vector<float> m_eLOC1_smt;        /// smoothed parameter eLOC1
  std::vector<float> m_ePHI_smt;         /// smoothed parameter ePHI
  std::vector<float> m_eTHETA_smt;       /// smoothed parameter eTHETA
  std::vector<float> m_eQOP_smt;         /// smoothed parameter eQOP
  std::vector<float> m_eT_smt;           /// smoothed parameter eT
  std::vector<float> m_res_eLOC0_smt;    /// smoothed parameter eLOC0 residual
  std::vector<float> m_res_eLOC1_smt;    /// smoothed parameter eLOC1 residual
  std::vector<float> m_err_eLOC0_smt;    /// smoothed parameter eLOC0 error
  std::vector<float> m_err_eLOC1_smt;    /// smoothed parameter eLOC1 error
  std::vector<float> m_err_ePHI_smt;     /// smoothed parameter ePHI error
  std::vector<float> m_err_eTHETA_smt;   /// smoothed parameter eTHETA error
  std::vector<float> m_err_eQOP_smt;     /// smoothed parameter eQOP error
  std::vector<float> m_err_eT_smt;       /// smoothed parameter eT error
  std::vector<float> m_pull_eLOC0_smt;   /// smoothed parameter eLOC0 pull
  std::vector<float> m_pull_eLOC1_smt;   /// smoothed parameter eLOC1 pull
  std::vector<float> m_x_smt;            /// smoothed global x
  std::vector<float> m_y_smt;            /// smoothed global y
  std::vector<float> m_z_smt;            /// smoothed global z
  std::vector<float> m_px_smt;           /// smoothed momentum px
  std::vector<float> m_py_smt;           /// smoothed momentum py
  std::vector<float> m_pz_smt;           /// smoothed momentum pz
  std::vector<float> m_eta_smt;          /// smoothed momentum eta
  std::vector<float> m_pT_smt;           /// smoothed momentum pT

};

#endif
