#!/usr/bin/env python

import sys
from AthenaCommon.Logging import log, logging
from AthenaCommon.Constants import DEBUG, VERBOSE, INFO
from AthenaCommon.Configurable import Configurable
from CalypsoConfiguration.AllConfigFlags import ConfigFlags
from AthenaConfiguration.TestDefaults import defaultTestFiles
from AthenaConfiguration.MainServicesConfig import MainServicesCfg
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
from AthenaPoolCnvSvc.PoolWriteConfig import PoolWriteCfg
from OutputStreamAthenaPool.OutputStreamConfig import OutputStreamCfg
from FaserActsGeometry.FaserActsExtrapolationConfig import FaserActsExtrapolationAlgCfg

# Set up logging and new style config
log.setLevel(DEBUG)
Configurable.configurableRun3Behavior = True

# Configure
#ConfigFlags.Input.Files = ["/cvmfs/atlas-nightlies.cern.ch/repo/data/data-art/esd/100evts10lumiblocks.ESD.root"]
#ConfigFlags.Output.RDOFileName = "myRDO_sp.pool.root"
ConfigFlags.Input.isMC = True                                # Needed to bypass autoconfig
ConfigFlags.IOVDb.GlobalTag = "OFLCOND-XXXX-XXX-XX"
ConfigFlags.GeoModel.FaserVersion = "FASER-01"               # Always needed
# Workaround for bug/missing flag; unimportant otherwise 
ConfigFlags.addFlag("Input.InitialTimeStamp", 0)
# Workaround to avoid problematic ISF code
ConfigFlags.GeoModel.Layout = "Development"
ConfigFlags.Detector.SimulateFaserSCT = True
#ConfigFlags.GeoModel.AtlasVersion = "ATLAS-R2-2016-01-00-01" # Always needed to fool autoconfig; value ignored
ConfigFlags.GeoModel.Align.Dynamic = False
#ConfigFlags.Concurrency.NumThreads = 1
#ConfigFlags.Beam.NumberOfCollisions = 0.
ConfigFlags.lock()

# Core components
acc = MainServicesCfg(ConfigFlags)
#acc.merge(PoolReadCfg(ConfigFlags))
#acc.merge(PoolWriteCfg(ConfigFlags))

# Inner Detector
acc.merge(FaserActsExtrapolationAlgCfg(ConfigFlags))

# Dump config
logging.getLogger('forcomps').setLevel(VERBOSE)
acc.foreach_component("*").OutputLevel = VERBOSE
acc.foreach_component("*ClassID*").OutputLevel = INFO
#acc.getCondAlgo("FaserActsAlignmentCondAlg").OutputLevel = VERBOSE
#acc.getCondAlgo("FaserNominalAlignmentCondAlg").OutputLevel = VERBOSE
acc.getService("StoreGateSvc").Dump = True
acc.getService("ConditionStore").Dump = True
acc.printConfig(withDetails=True)
ConfigFlags.dump()
# Execute and finish
sc = acc.run(maxEvents=1)
# Success should be 0
sys.exit(not sc.isSuccess())
