/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/

#include "FaserActsGeometry/FaserActsTrackingGeometrySvc.h"

// ATHENA
#include "TrackerReadoutGeometry/SCT_DetectorManager.h"
#include "StoreGate/StoreGateSvc.h"
#include "GaudiKernel/EventContext.h"
#include "TrackerIdentifier/FaserSCT_ID.h"

// ACTS
#include "Acts/Geometry/GeometryContext.hpp"
#include "Acts/Geometry/TrackingGeometry.hpp"
#include "Acts/Geometry/ITrackingVolumeBuilder.hpp"
#include "Acts/Geometry/LayerArrayCreator.hpp"
#include "Acts/Geometry/SurfaceArrayCreator.hpp"
#include "Acts/Geometry/LayerCreator.hpp"
#include "Acts/Geometry/TrackingVolumeArrayCreator.hpp"
#include "Acts/Geometry/CylinderVolumeHelper.hpp"
#include "Acts/Geometry/TrackingGeometryBuilder.hpp"
#include "Acts/Geometry/CylinderVolumeBuilder.hpp"
//#include "Acts/Geometry/CuboidVolumeBuilder.hpp"
#include "FaserActsGeometry/CuboidVolumeBuilder.h"
#include "Acts/ActsVersion.hpp"
#include "Acts/Utilities/Units.hpp"

// PACKAGE
#include "FaserActsGeometry/FaserActsLayerBuilder.h"
#include "FaserActsGeometry/FaserActsDetectorElement.h"
#include "FaserActsGeometry/FaserActsAlignmentStore.h"
#include "FaserActsGeometry/FaserActsGeometryContext.h"
#include "ActsInterop/IdentityHelper.h"
#include "ActsInterop/Logger.h"

using namespace Acts::UnitLiterals;

FaserActsTrackingGeometrySvc::FaserActsTrackingGeometrySvc(const std::string& name, ISvcLocator* svc)
   : base_class(name,svc),
   m_detStore("StoreGateSvc/DetectorStore", name)
{
  m_elementStore = std::make_shared<std::vector<std::shared_ptr<const FaserActsDetectorElement>>>();
}

StatusCode
FaserActsTrackingGeometrySvc::initialize()
{
  ATH_MSG_INFO(name() << " is initializing");
  ATH_MSG_INFO("Acts version is: v" << Acts::VersionMajor << "."
                                    << Acts::VersionMinor << "."
                                    << Acts::VersionPatch
                                    << " [" << Acts::CommitHash << "]");

  ATH_CHECK ( m_detStore->retrieve(p_SCTManager, "SCT") );

  Acts::LayerArrayCreator::Config lacCfg;
  auto layerArrayCreator = std::make_shared<const Acts::LayerArrayCreator>(
      lacCfg, makeActsAthenaLogger(this, "LayArrCrtr", "ActsTGSvc"));

  Acts::TrackingVolumeArrayCreator::Config tvcCfg;
  auto trackingVolumeArrayCreator
      = std::make_shared<const Acts::TrackingVolumeArrayCreator>(
          tvcCfg, makeActsAthenaLogger(this, "TrkVolArrCrtr", "ActsTGSvc"));

  Acts::CylinderVolumeHelper::Config cvhConfig;
  cvhConfig.layerArrayCreator          = layerArrayCreator;
  cvhConfig.trackingVolumeArrayCreator = trackingVolumeArrayCreator;

  auto cylinderVolumeHelper
    = std::make_shared<const Acts::CylinderVolumeHelper>(
        cvhConfig, makeActsAthenaLogger(this, "CylVolHlpr", "ActsTGSvc"));

  Acts::TrackingGeometryBuilder::Config tgbConfig;
  try {
    // SCT
    tgbConfig.trackingVolumeBuilders.push_back([&](
          const auto& gctx, const auto& inner, const auto&) {
        auto tv = makeVolumeBuilder(gctx, p_SCTManager);
        return tv->trackingVolume(gctx);
    });

  }
  catch (const std::invalid_argument& e) {
    ATH_MSG_ERROR(e.what());
    return StatusCode::FAILURE;
  }


  auto trackingGeometryBuilder
      = std::make_shared<const Acts::TrackingGeometryBuilder>(tgbConfig,
          makeActsAthenaLogger(this, "TrkGeomBldr", "ActsTGSvc"));


  // default geometry context, this is nominal
  FaserActsGeometryContext constructionContext;
  constructionContext.construction = true;

  m_trackingGeometry = trackingGeometryBuilder
    ->trackingGeometry(constructionContext.any());

  ATH_MSG_VERBOSE("Building nominal alignment store");
  FaserActsAlignmentStore* nominalAlignmentStore = new FaserActsAlignmentStore();

  populateAlignmentStore(nominalAlignmentStore);

  // manage ownership
  m_nominalAlignmentStore = std::unique_ptr<const FaserActsAlignmentStore>(nominalAlignmentStore);

  ATH_MSG_INFO("Acts TrackingGeometry construction completed");

  return StatusCode::SUCCESS;
}

std::shared_ptr<const Acts::TrackingGeometry>
FaserActsTrackingGeometrySvc::trackingGeometry() {

  ATH_MSG_VERBOSE("Retrieving tracking geometry");
  return m_trackingGeometry;
}

std::shared_ptr<const Acts::ITrackingVolumeBuilder>
FaserActsTrackingGeometrySvc::makeVolumeBuilder(const Acts::GeometryContext& gctx, const TrackerDD::SCT_DetectorManager* manager)
{
  std::string managerName = manager->getName();

  std::shared_ptr<FaserActsLayerBuilder> gmLayerBuilder;

  FaserActsLayerBuilder::Config cfg;
  cfg.subdetector = FaserActsDetectorElement::Subdetector::SCT;
  cfg.mng = static_cast<const TrackerDD::SCT_DetectorManager*>(manager);
  cfg.elementStore = m_elementStore;
  gmLayerBuilder = std::make_shared<FaserActsLayerBuilder>(cfg,
      makeActsAthenaLogger(this, "GMLayBldr", "ActsTGSvc"));
  auto cvbConfig = gmLayerBuilder->buildVolume(gctx);
  auto cvb = std::make_shared<const FaserActs::CuboidVolumeBuilder>(cvbConfig);

  return cvb;
}

void
FaserActsTrackingGeometrySvc::populateAlignmentStore(FaserActsAlignmentStore *store) const
{
  // populate the alignment store with all detector elements
  m_trackingGeometry->visitSurfaces(
    [store](const Acts::Surface* srf) {
    const Acts::DetectorElementBase* detElem = srf->associatedDetectorElement();
    const auto* gmde = dynamic_cast<const FaserActsDetectorElement*>(detElem);
    gmde->storeTransform(store);
  });
}

const FaserActsAlignmentStore*
FaserActsTrackingGeometrySvc::getNominalAlignmentStore() const
{
  return m_nominalAlignmentStore.get();
}
