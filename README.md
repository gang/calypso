Calypso is the FASER offline software system, based on the ATLAS Athena and LHCb GAUDI frameworks

Start by creating a personal fork of https://gitlab.cern.ch/faser/calypso.git (using the gitlab web interface)

Then the following sequence will allow you to compile and run Calypso on any machine with cvmfs access.
```
#clone the (forked) project to your local machine
git clone --recursive https://:@gitlab.cern.ch:8443/$USERNAME/calypso.git 

#The next three lines are used to setup the ATLAS release environment
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase 
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup --input=calypso/asetup.faser master,latest,Athena

#create build directory
mkdir build
cd build

#build calypso
cmake -DCMAKE_INSTALL_PREFIX=../run ../calypso ; make ; make install

It can be convenient to alias the "asetup --input=calypso/asetup.faser" to something like "fsetup"
