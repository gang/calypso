#
#  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
from AthenaConfiguration.ComponentFactory import CompFactory
#
#  Manager classes for detector geometry and sensitive detectors
#
from G4FaserServices.G4FaserServicesConfigNew import DetectorGeometrySvcCfg, PhysicsListSvcCfg
from G4FaserTools.G4FaserToolsConfigNew import SensitiveDetectorMasterToolCfg
from G4FaserServices.G4FaserUserActionConfigNew import UserActionSvcCfg
# from G4AtlasApps.G4Atlas_MetadataNew import writeSimulationParametersMetadata
#
#  Framework utilities
#
from FaserISF_Services.FaserISF_ServicesConfigNew import FaserTruthServiceCfg, FaserInputConverterCfg, FaserGeoIDSvcCfg
#
# from  G4FaserAlg.G4FaserAlgConf import G4FaserAlg
G4FaserAlg=CompFactory.G4FaserAlg

def G4FaserAlgCfg(ConfigFlags, name='G4FaserAlg', **kwargs):
    #
    #  add Services to G4AtlasAlg
    #
    result = DetectorGeometrySvcCfg(ConfigFlags)
    kwargs.setdefault('DetGeoSvc', result.getService("DetectorGeometrySvc"))
    #
    #  MC particle container names
    #
    kwargs.setdefault("InputTruthCollection", "GEN_EVENT") 
    kwargs.setdefault("OutputTruthCollection", "TruthEvent")
    #
    #  Option to free memory by dropping GeoModel after construction
    #
    # if ConfigFlags.Sim.ReleaseGeoModel:
    kwargs.setdefault('ReleaseGeoModel', ConfigFlags.Sim.ReleaseGeoModel)
    #
    #  Record the particle flux during the simulation
    #
    #if ConfigFlags.Sim.RecordFlux:
    kwargs.setdefault('RecordFlux' , ConfigFlags.Sim.RecordFlux)
    #
    #  Treatment of bad events
    #
    # if ConfigFlags.Sim.FlagAbortedEvents:
        ## default false
    kwargs.setdefault('FlagAbortedEvents' ,ConfigFlags.Sim.FlagAbortedEvents)
    if ConfigFlags.Sim.FlagAbortedEvents and ConfigFlags.Sim.KillAbortedEvents:
            print('WARNING When G4AtlasAlg.FlagAbortedEvents is True G4AtlasAlg.KillAbortedEvents should be False!!! Setting G4AtlasAlg.KillAbortedEvents = False now!')
            kwargs.setdefault('KillAbortedEvents' ,False)
    # if  ConfigFlags.Sim.KillAbortedEvents:
        ## default true
    kwargs.setdefault('KillAbortedEvents' ,ConfigFlags.Sim.KillAbortedEvents)
    #
    #  Random numbers
    #
    from RngComps.RandomServices import AthEngines, RNG
    if ConfigFlags.Random.Engine in AthEngines.keys():
        result.merge(RNG(ConfigFlags.Random.Engine, name="AthRNGSvc"))
        kwargs.setdefault("AtRndmGenSvc",result.getService("AthRNGSvc"))

    kwargs.setdefault("RandomGenerator", "athena")
    #
    #  Multi-threading settings
    #
    is_hive = ConfigFlags.Concurrency.NumThreads > 1
    kwargs.setdefault('MultiThreading', is_hive)
    #
    #  What truth information to save?
    #
    accMCTruth = FaserTruthServiceCfg(ConfigFlags)
    result.merge(accMCTruth)
    kwargs.setdefault('TruthRecordService', result.getService("FaserISF_TruthService"))
    #kwargs.setdefault('TruthRecordService', ConfigFlags.Sim.TruthStrategy) # TODO need to have manual override (simFlags.TruthStrategy.TruthServiceName())
    #
    #  Locates detector region for space points (no longer used by G4FaserAlg)
    #
    accGeoID = FaserGeoIDSvcCfg(ConfigFlags)
    result.merge(accGeoID)
    #kwargs.setdefault('GeoIDSvc', result.getService('ISF_FaserGeoIDSvc'))
    #
    #  Converts generator particles to a proprietary type managed by ISF
    #
    accInputConverter = FaserInputConverterCfg(ConfigFlags)
    result.merge(accInputConverter)
    kwargs.setdefault('InputConverter', result.getService("ISF_FaserInputConverter"))
    #
    # Sensitive detector master tool
    #
    accSensitiveDetector = SensitiveDetectorMasterToolCfg(ConfigFlags)
    result.merge(accSensitiveDetector)
    kwargs.setdefault('SenDetMasterTool', result.getPublicTool("SensitiveDetectorMasterTool")) #NOTE - is still a public tool
    #
    #Write MetaData container
    #
    #result.merge(writeSimulationParametersMetadata(ConfigFlags))
    #
    #User action services (Slow...)
    #
    result.merge( UserActionSvcCfg(ConfigFlags) )
    kwargs.setdefault('UserActionSvc', result.getService( "G4UA::UserActionSvc") )

    #PhysicsListSvc
    result.merge( PhysicsListSvcCfg(ConfigFlags) )
    kwargs.setdefault('PhysicsListSvc', result.getService( "PhysicsListSvc") )

    #
    #  Output level
    #
    ## G4AtlasAlg verbosities (available domains = Navigator, Propagator, Tracking, Stepping, Stacking, Event)
    ## Set stepper verbose = 1 if the Athena logging level is <= DEBUG
    # TODO: Why does it complain that G4AtlasAlgConf.G4AtlasAlg has no "Verbosities" object? Fix.
    verbosities=dict(Placeholder = '0')
    if "OutputLevel" in kwargs and kwargs["OutputLevel"] <= 2 :
       verbosities["Tracking"]='1'
       print (verbosities)
    kwargs.setdefault('Verbosities', verbosities)
    #
    # Set commands for the G4FaserAlg
    #
    kwargs.setdefault("G4Commands", ConfigFlags.Sim.G4Commands)

    result.addEventAlgo(G4FaserAlg(name, **kwargs))
    return result

