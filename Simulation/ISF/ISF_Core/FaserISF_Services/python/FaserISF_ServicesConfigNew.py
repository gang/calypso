# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration

"""
Service configurations for ISF
KG Tan, 17/06/2012
"""

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from BarcodeServices.BarcodeServicesConfigNew import MC15aPlusBarcodeSvcCfg
from ISF_HepMC_Tools.ISF_HepMC_ToolsConfigNew import ParticleFinalStateFilterCfg, GenParticleInteractingFilterCfg
#from FaserISF_HepMC_Tools.FaserISF_HepMC_ToolsConfigNew import FaserParticlePositionFilterCfg, FaserParticleGenericFilterCfg
from FaserISF_HepMC_Tools.FaserISF_HepMC_ToolsConfigNew import FaserTruthStrategyCfg, FaserDipoleTruthStrategyCfg

# from ISF_HepMC_Tools.ISF_HepMC_ToolsConfigNew import TruthStrategyGroupID_MC15Cfg, TruthStrategyGroupCaloMuBremCfg, TruthStrategyGroupCaloDecay_MC15Cfg, TruthStrategyGroupIDHadInt_MC15Cfg, ParticleFinalStateFilterCfg, ParticlePositionFilterDynamicCfg, EtaPhiFilterCfg, GenParticleInteractingFilterCfg
# from SubDetectorEnvelopes.SubDetectorEnvelopesConfigNew import EnvelopeDefSvcCfg


ISF__FaserTruthSvc, ISF__FaserGeoIDSvc, ISF__FaserInputConverter = CompFactory.getComps("ISF::FaserTruthSvc","ISF::FaserGeoIDSvc","ISF::FaserInputConverter")

#Functions yet to be migrated:
#getParticleBrokerSvcNoOrdering, getParticleBrokerSvc, getAFIIParticleBrokerSvc, getAFIIEnvelopeDefSvc, getAFIIGeoIDSvc
#getLongLivedInputConverter, getValidationTruthService, getMC12BeamPipeTruthStrategies, getMC12IDTruthStrategies
#getMC12CaloTruthStrategies, getMC12MSTruthStrategies, getMC12TruthService, getTruthService, getMC12LLPTruthService, getMC12PlusTruthService,  getMC15IDTruthStrategies
#getMC15CaloTruthStrategies


def FaserGeoIDSvcCfg(ConfigFlags, name="ISF_FaserGeoIDSvc", **kwargs):
    result = ComponentAccumulator()

    result.addService(ISF__FaserGeoIDSvc(name, **kwargs))

    return result


def GenParticleFiltersToolCfg(ConfigFlags):
    result = ComponentAccumulator()

    #acc1 = ParticlePositionFilterDynamicCfg(ConfigFlags)
    genParticleFilterList = []

    acc1 = ParticleFinalStateFilterCfg(ConfigFlags)
    genParticleFilterList += [result.popToolsAndMerge(acc1)]

    # acc2 = ParticlePositionFilterDynamicCfg(ConfigFlags)
    # genParticleFilterList += [result.popToolsAndMerge(acc2)]

    # acc3 = EtaPhiFilterCfg(ConfigFlags)
    # genParticleFilterList += [result.popToolsAndMerge(acc3)]

    acc4 = GenParticleInteractingFilterCfg(ConfigFlags)
    genParticleFilterList += [result.popToolsAndMerge(acc4)]

    result.setPrivateTools(genParticleFilterList)
    return result


def FaserInputConverterCfg(ConfigFlags, name="ISF_FaserInputConverter", **kwargs):
    result = ComponentAccumulator()

    #just use this barcodeSvc for now. TODO - make configurable
    #from G4AtlasApps.SimFlags import simFlags
    #kwargs.setdefault('BarcodeSvc', simFlags.TruthStrategy.BarcodeServiceName())
    result = MC15aPlusBarcodeSvcCfg(ConfigFlags)
    kwargs.setdefault('BarcodeSvc', result.getService("Barcode_MC15aPlusBarcodeSvc") )

    kwargs.setdefault("UseGeneratedParticleMass", False)

    acc_GenParticleFiltersList = GenParticleFiltersToolCfg(ConfigFlags)
    kwargs.setdefault("GenParticleFilters", result.popToolsAndMerge(acc_GenParticleFiltersList) )

    result.addService(ISF__FaserInputConverter(name, **kwargs))
    return result

#
# Generic Truth Service Configurations
#
def FaserTruthServiceCfg(ConfigFlags, name="FaserISF_TruthService", **kwargs):
    result = MC15aPlusBarcodeSvcCfg(ConfigFlags)
    kwargs.setdefault('BarcodeSvc', result.getService("Barcode_MC15aPlusBarcodeSvc") )
    
    acc = FaserTruthStrategyCfg(ConfigFlags)
    acc2= FaserDipoleTruthStrategyCfg(ConfigFlags)
    kwargs.setdefault('TruthStrategies',[result.popToolsAndMerge(acc), result.popToolsAndMerge(acc2)])

    kwargs.setdefault('SkipIfNoChildren', True)
    kwargs.setdefault('SkipIfNoParentBarcode', True)

    import PyUtils.RootUtils as rootUtils
    ROOT = rootUtils.import_root()

    import cppyy
    cppyy.load_library('FaserDetDescrDict')
    from ROOT.FaserDetDescr import FaserRegion

    kwargs.setdefault('ForceEndVtxInRegions', [FaserRegion.fFaserNeutrino,
                                               FaserRegion.fFaserScintillator,
                                               FaserRegion.fFaserTracker,
                                               FaserRegion.fFaserDipole,
                                               FaserRegion.fFaserCalorimeter,
                                               FaserRegion.fFaserCavern])
                                               
    #long_lived_simulators = ['LongLived', 'longLived', 'QS']
    #from ISF_Config.ISF_jobProperties import ISF_Flags
    #is_long_lived_simulation = any(x in ISF_Flags.Simulator() for x in long_lived_simulators) #FIXME this should be set in a nicer way.
    is_long_lived_simulation = True
    if is_long_lived_simulation:
        kwargs.setdefault('QuasiStableParticlesIncluded', True)

    result.addService(ISF__FaserTruthSvc(name, **kwargs))
    return result


# def MC15TruthServiceCfg(ConfigFlags, name="ISF_MC15TruthService", **kwargs):
#     result = ComponentAccumulator()
#     # importing Reflex dictionary to access AtlasDetDescr::AtlasRegion enum
#     import ROOT, cppyy
#     cppyy.loadDictionary('AtlasDetDescrDict')
#     AtlasRegion = ROOT.AtlasDetDescr

#     acc1 = TruthStrategyGroupID_MC15Cfg(ConfigFlags)
#     acc2 = TruthStrategyGroupIDHadInt_MC15Cfg(ConfigFlags)
#     acc3 = TruthStrategyGroupCaloMuBremCfg(ConfigFlags)
#     acc4 = TruthStrategyGroupCaloDecay_MC15Cfg(ConfigFlags)

#     kwargs.setdefault('TruthStrategies', [result.popToolsAndMerge(acc1),
#                                           result.popToolsAndMerge(acc2),
#                                           result.popToolsAndMerge(acc3), #FIXME this should be ISF_MCTruthStrategyGroupCaloMuBrem_MC15!!
#                                           result.popToolsAndMerge(acc4)])


#     kwargs.setdefault('IgnoreUndefinedBarcodes', False)
#     kwargs.setdefault('PassWholeVertices', False) # new for MC15 - can write out partial vertices.
#     kwargs.setdefault('ForceEndVtxInRegions', [AtlasRegion.fAtlasID])
#     accTruthService = GenericTruthServiceCfg(ConfigFlags, name, **kwargs)
#     result.merge(accTruthService)
#     return result


# def MC15aPlusTruthServiceCfg(ConfigFlags, name="ISF_MC15aPlusTruthService", **kwargs):
#     # importing Reflex dictionary to access AtlasDetDescr::AtlasRegion enum
#     import ROOT, cppyy
#     cppyy.loadDictionary('AtlasDetDescrDict')
#     AtlasRegion = ROOT.AtlasDetDescr
#     kwargs.setdefault('ForceEndVtxInRegions', [AtlasRegion.fAtlasID])
#     result = MC15TruthServiceCfg(ConfigFlags, name, **kwargs)
#     return result
