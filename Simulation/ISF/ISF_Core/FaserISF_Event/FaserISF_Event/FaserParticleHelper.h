/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

///////////////////////////////////////////////////////////////////
// FaserParticleHelper.h, (c) ATLAS Detector software
///////////////////////////////////////////////////////////////////

#ifndef FASERISF_EVENT_FASERPARTICLEHELPER_H
#define FASERISF_EVENT_FASERPARTICLEHELPER_H 1

// forward declarations
namespace HepMC {
  class GenParticle;
}

namespace ISF {

  class FaserISFParticle;

  /**
   @class FaserParticleHelper

   A simple helper class to perform commonly used, basic functions, like
     * converting FaserISFParticles into a differnt particle representation format

   @author Elmar.Ritsch -at- cern.ch
   */

  class FaserParticleHelper {

    public:
        /** empty constructor */
        FaserParticleHelper() {} ;

        /** empty destructor */
        ~FaserParticleHelper() {} ;

        /** convert the given particle to HepMC format */
        static HepMC::GenParticle *convert( const ISF::FaserISFParticle &p);
        
    private :
  };

} // end of namespace

/* implementation for inlined methods (none existing at the moment) */
//#include <FaserISF_Event/FaserParticleHelper.icc>

#endif // FASERISF_EVENT_FASERPARTICLEHELPER_H

